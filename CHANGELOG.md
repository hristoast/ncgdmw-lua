## NCGDMW Lua Edition Changelog

#### 3.6.3

* Fixed NCGD not enabled if installed mid-game

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/36197916) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.6.2

* Fixed stats menu not showing
* Cleared 0.48 related info from the FAQ

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/36096478) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.6.1

* Fixed skill use type settings not accepting decimals

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/36038571) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.6.0

* Support for OpenMW 0.48 has been removed
* Highly content-agnostic CharGen completion detection has been implemented. This means that special plugins for various total conversions are no longer needed. The two main NCGDMW Lua Edition plugins should now fully work for Morrowind and all TCs!
* Balanced the base gain for multiple skills and their different use types
* New settings to configure skill use type gains (per skill, per use type)
* Fixed MBSP error when no spells are selected (thx to @fallchildren)
* Document multi-school spell skill gains (#45)

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/35985988) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.5.5

* Message boxes now set `showInDialogue = false` to prevent them from appearing in dialogue windows (OpenMW 0.49 only)
* Using multi-school spells now progresses all relevant skills proportionally to the theoretical cost of each school effects (independent of MBSP, OpenMW 0.49 only)
* Fixed skill progression reset on game load for skills above 100
* Fixed settings page layout sometimes broken when adding individual uncappers
* Improved uncappers renderer
* Renamed internal events to prevent collisions with other mods

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/35432978) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.5.4

* Updated patch for Birthsigns Respected

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/33229934) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.5.3

* MBSP: Fixed 1 magicka cost spells not contributing to experience on openmw 0.48
* Removed "Increase magic damage taken" feature as it doesn't really fit in NCGD. It's now available in [Harder Better Faster Stronger](https://www.nexusmods.com/morrowind/mods/55542) mod

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/33217588) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.5.2

* Fixed fortify health effects cancelled when injured with magic, and increase magic damage setting is enabled

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/32273630) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.5.1

* Fixed ignored fortify luck abilities
* Fixed wrong base attributes computations on character generation
* Fixed 2 Russian locales where double quotes were not escaped

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/29665108) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.5

* Reorganized folders and plugins to ease the comprehension and installation process
* Improved installation documentation
* Folder numeral prefixes now match the standard rule: Same number means exclusive folders
* Removed old classic NCGDMW plugin
* Removed residual changes to fAthleticsRunBonus and fBlockStillBonus GMSTs
* **OpenMW 0.48**: Allow enabling/disabling slower skill growth for Alt Start and Starwind plugins ([#42](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/42))

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/29003647) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.4.4

* Added chart in profile spreadsheet to compute skill gain reductions based on settings and skill levels
* **OpenMW 0.49 only**: Re-calculate starter spells after NCGD initialization to take into account updated attributes ([#40](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/40))
* Fixed specialization skills increasing too slowly with Starwind and AltStart plugins
* Fixed major, minor and misc skills increasing at half speed with Starwind and AltStart plugins
* Exclude fortify attribute abilities from attributes start values
* Fixed first attribute change messages always saying that attributes have increased just after character generation
* Removed some spamming debug messages

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/28980859) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.4.3

* Added the recent standalone MBSP as a non-compatible mod

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/27265076) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.4.2

* Improved attributes and skills decrease handling when changing uncappers
* Updated Russian locales (thanks to [@KorInTor](https://gitlab.com/KorInTor))
* Fixed "increase magic damage taken" feature not disabled in god mode
* Improved missing or invalid plugin checks (only for OpenMW 0.49)

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/27144458) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.4.1

* When changing settings, only update player's profile when game resumes
* Fixed broken decay when sleeping

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/26584392) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.4

* New interface to change skill affected attributes ([#31](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/31))
* New interfaces to change skill value and skill progress
* Added tooltips to stats menu to explain attributes and skills values (only in inventory mode when mouse pointer is visible)
* New spreadsheet file allowing the computation of attributes growth, player level, and level progress based on starting skills and attributes, and skill levels up
* New magicka refund formula, more generous
* Attribute and skill uncappers are now defined as max values
* New settings to add per skill or attribute uncapper values ([#39](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/39))
* Disable decay and refund by default
* Improved settings code
* Reduced decay progress reduction per skill used from 6 to 1 hour as decay was too slow
* Fixed a packaging error that caused the Starwind plugin to not be added to the zip
* Fixed error messages when a plugin is missing and fixed error window layout ([#37](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/37))

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/26534078) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.3

* New setting to restore vanilla attributes capper ([#35](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/35))
* New messages log in the stats menu ([#22](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/22))
* **OpenMW 0.49 only**: New setting to increase magic damage taken
* Stats menu: Added attributes starting, growth and current values; Added skills current values
* Stats menu: Added game time and days passed
* Stats menu: Special color for decayed skill values, progressive color change for decay percents
* Decay: Faster recovering of skill lost levels
* Decay memory now based on skill level instead of player level and intelligence
* Decay now slowed down on each skill used, in addition to skill level up
* Decay also slightly slowed down, on skill used, by synergy, for other skills of same specialization
* Decay time passed reduced or cancelled when resting with or without a bed, or when using a transport (degraded feature on OpenMW 0.48)
* Training a skill now cancels decay progress of that skill and halves decay time passed for other skills of same specialization
* New configuration.lua file where main default values can be changed
* Allow localization of starwind names
* Fixed Lua errors happening before NCGDMW initialization
* Fixed accelerated decay in some cases
* Fixed profile reset not resetting total base attributes leading to the need of multiple player levels up to go above level 1
* Fixed player profile not updated right after a full skill increase (books, trainers)
* Removed/simplified/factorized code
* Added a patch for use with OpenMW 0.48 and [Birthsigns RESPECted - A Birthsign Rebalance Mod](https://www.nexusmods.com/morrowind/mods/54542)

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/25734544) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.2

* **OpenMW 0.49 only**: Remove the need of patches that convert ability spells into curses
* **OpenMW 0.49 only**: Added a reset stats function to NCGDMW interface, useful for upgrading games saved on versions prior to NCGDMW 3.2
* **OpenMW 0.49 only**: Show base attributes and skills modifiers in the stats menu
* **OpenMW 0.49 only**: Improved NCGD initialization to better handle adding the mod mid-game
* Fixed broken decay when enabled later in game
* Show fortify health value from abilities in the stats menu
* Added player level to stats menu
* Preserve fortify health abilities effects. With OpenMW 0.48, only works on new games and with non toggable abilities
* Fixed attributes external changes doubled
* Fixed HP not updated after changing related settings
* Fixed debugPrint crash on nil values
* Growth and decay rates computations don't rely on locales anymore
* Factorized health attributes code

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/25156880) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.1

* Fixed a potential issue with loading the last update on an existing save

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/24328968) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 3.0

* Fixed skill gain broken after Lua API update (Credit: [@Draxiss](https://gitlab.com/Draxiss314))
* Merged NCGD and MBSP settings pages (Credit: [@mym](https://gitlab.com/mym34))
* Merged NCGD stats menu and skill progress menu (Credit: [@mym](https://gitlab.com/mym34))
* New setting to disable MBSP (Credit: [@mym](https://gitlab.com/mym34))
* Added missing locales for MBSP (Credit: [@mym](https://gitlab.com/mym34))
* Reorganized, factorized and simplified code (Credit: [@mym](https://gitlab.com/mym34))
* Fixed wrong skill progression if MBSP is enabled (Credit: [@mym](https://gitlab.com/mym34))
* Fixed birthsigns with fortify attribute effects not taken into account during init (Credit: [@mym](https://gitlab.com/mym34))
* New feature for openmw 0.49: New setting that allows skill gain excess to be carried over to next level (Credit: [@mym](https://gitlab.com/mym34))
* New feature: New setting that allows you to change your base and per level HP ratio (Credit: [@mym](https://gitlab.com/mym34))
* Fixed a bug that made the Alt-Start for total conversions plugin unusable ([#34](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/34))
* Rewrote the README to hopefully better explain everything

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/24290203) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 2.2

* Optimized the code a bit (Credit: [@mym](https://gitlab.com/mym34))
* **OpenMW 0.49 only**: MBSP will use the new skill progression interface rather than trying to do work every frame (Credit: [@mym](https://gitlab.com/mym34))
* Added a fork of [MBSP Uncapped (OpenMW Lua) - NCGD Compat](https://www.nexusmods.com/morrowind/mods/53064) which includes some fixes for OpenMW 0.49 dev builds
* **OpenMW 0.49 only**: Skill progression is now configurable, see the README for full details (Credit: [@mym](https://gitlab.com/mym34))
* The amount of HP gained per level is now much more configurable, see the README for full details (Credit: [@mym](https://gitlab.com/mym34))
* **OpenMW 0.49 only**: Skill increases from books may now be disabled (Credit: [@mym](https://gitlab.com/mym34))
* **OpenMW 0.49 only**: Birthsigns are now reloaded when a save game loads. This allows for mods that change birthsign behavior to work after starting a new game (Credit: [@mym](https://gitlab.com/mym34))
* Added the `00 OpenMW-DevBuild` option for use with OpenMW 0.49 dev builds
  * This is a separate option in order to maintain backwards compatibility with the current stable release
  * When OpenMW 0.49 releases, this code will be moved into the main mod and 0.48 will be unsupported

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/23666951) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 2.1

* Reverted the "special skill bonus" for `03 NewNCGDDoubleSkillBonuses` (thanks Mehdi Yousfi-Monod!)
* Improved level calculation to be more responsive (now updates as needed when skills change -- thanks Mehdi Yousfi-Monod!)

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/22245390) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 2.0

* **OpenMW 0.49 only**: Jail time is no longer counted towards decay time ([#9](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/9))
* **OpenMW 0.49 only**: Vanilla birthsigns are automatically fixed via Lua! The mod init process will switch the vanilla abilities out for curses as needed
* **OpenMW 0.49 only**: Use the thick border template for the stats menu
* **OpenMW 0.49 only**: Automatically enable Starwind Names when the starwind plugin is loaded
* Added the `ncgdmw-vanilla-birthsigns-patch.omwaddon` plugin which edits "The Lady", "The Lover", and "The Steed" birthsigns found in the vanilla game to be "Curses" instead of an "Ability", thus reflecting their fortifactions more accurately in the final stat value (**OpenMW 0.48 only**)
  * **Unfortunately** doing this breaks the birthsign UI since it is hardcoded to look for abilities. **You can avoid the issue by using OpenMW 0.49 or newer!**
  * Any birthsign mod that provides a birthsign which fortifies an attribute or skill should provide a compatibility version that uses "Curses"
* Added an option for state-based HP, off by default ([#13](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/13))
* Decay is now only calculated based on the amount of time decay was active, not while it was disabled ([#21](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/21))
* Added text to the README that hopefully better explains what the mod is and how it works at a high level
* New packaging format and better documentation of the various options
* Added patches for various mods by Alvazir
* Added new health and leveling calculations by [Mehdi Yousfi-Monod](https://gitlab.com/mym34)
* Added SV localization (thanks Lysol!)
* Small code optimizations (only check for the API version once)
* Added an option to disable the intro message

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/21213261) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 1.3

* **OpenMW 0.49 only**: Play a sound when decay happens
* Added the `debugMode` option which enables extra debugging info in the console.
* Fixed a bug that broke the decay stats menu. ([#20](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/20))
* Fixed a bug where decay progress wouldn't halve when you raised a skill. ([#19](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/19))
* Fixed typo that broke getting the decay rate. ([#20](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/20))

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/16901014) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 1.2

* Fixed typo that could break the stats menu for Starwind players

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/15913322) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 1.1

* The mod now updates when the UI is opened and can immediately see when barter and/or speechcraft are raised (`onUpdate` -> `onFrame` under the hood; latency is critical for this mod so it is OK to use this engine handler).
* When used with OpenMW 0.49 or newer this mod can now directly examine loaded plugins to ensure that the required `.omwaddon` is loaded (versus the prior method of looking at GMST values, which will still be used for OpenMW 0.48).
* Fixed a problem where external changes to Luck were not preserved ([#14](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/14)).

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/15883628) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### 1.0

* The mod is now considered out of beta! This is simply a re-release of beta12 with no changes.

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/15708304) | [Nexus](https://www.nexusmods.com/morrowind/mods/53136)

#### beta12

* Fixed incorrect handling of leveling down to 1

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/15383561)

#### beta11

* Fixed more incorrect Starwind skill names

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/11285260)

#### beta10

* Fixed an incorrect Starwind skill name that I missed

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/11255731)

#### beta9

* Fixed a bug that caused the alt start/Starwind plugins to infinitely spawn potions

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/11083945)

#### beta8

* Fixed some incorrect Starwind skill names

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/11037746)

#### beta7

* The stats menus now update while you have them open.
* An error is now shown in the in-game UI when a required `.omwaddon` file isn't detected.

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/10564152)

#### beta6

* The alt start plugin now doesn't depend on Morrowind master files, in order to support games that don't depend on `Morrowind.esm` and friends.
* Added a specific plugin for [**Starwind**](https://www.nexusmods.com/morrowind/mods/48909) that doesn't provide descriptions for skills; the existing alt-start plugin can be used for other non-Morrowind games or alternative chargen mods.
* Added support for Starwind skill names in the NCGD skill decay menu

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/9792120)

#### beta5

* Added support for [**Starwind**](https://www.nexusmods.com/morrowind/mods/48909) (as well as any other non-Morrowind game or non-vanilla chargen mod) via an "alt start" plugin (credit goes to Greywander for the original alt start implementation)
  * This will place a potion in your inventory that you can drink to activate NCGDMW
  * Drink this potion **after** you've selected your race, class, and sign
* `build.sh` will now fail if we try to zip something that isn't there (`zip --must-match ...`)
* Correctly implemented the PL and PT_BR localizations
* Removed outdated interface documentation
* Added contact information to the README

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/8354345)

#### beta4

* Support external changes to attributes (for instance if another mod raises them, or if the player becomes a vampire) on top of NCGDMW's own calculations
* Fixed a bug that caused extremely rapid decay rates
* Added a note about mod compatibility with NCGDMW Lua Edition and Ability vs Curse spell types
* Updated the FAQ entry about going past 100 to clarify the current state of that
* Removed `Skill()` from the interface
* Added a note about this history of NCGDMW to the FAQ

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/7514203)

#### beta3

* Support new YAML-formatted localizations
* Added localizations for BR
* Added an interface for other mods to interact with NCGDMW
* Added documentation for events and the interface to the website
* Updated the mod settings menu to use the new builtin renderers provided by OpenMW
* The decay stats menu has a new layout and displays additional information
* The stats menu (with or without decay) will now display your [Marksman's Eye](https://modding-openmw.gitlab.io/marksmans-eye/) level, if you're also using that mod

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/7081151)

#### beta2

* Fixed a crash on skill up ([#1](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/1))
* Fixed health adjustments not applying after saving and reloading ([#2](https://gitlab.com/modding-openmw/ncgdmw-lua/-/issues/2))
* Changed the layout of the zip file a bit
* Added changelog and faq pages to the website

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/6074455)

#### beta1

Initial public release, now compatible with the latest code in OpenMW 0.48 dev builds.

No new features were added for this release; the only changes involved porting code for updated APIs.

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/5829792)

#### alpha1

Initial release of the mod, written for WIP code that had not yet been merged into OpenMW.

[Download Link](https://gitlab.com/modding-openmw/ncgdmw-lua/-/packages/5356693)
