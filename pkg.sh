#!/bin/sh
set -eu
modname=ncgdmw-lua
file_name=$modname.zip

cat > $modname-metadata.toml <<EOF
[package]
name = "NCGDMW Lua Edition"
description = "A "Natural Grow" leveling mod where your attributes and level grow automatically as your skills increase, and your skills will also decay over time (optional)."
homepage = "https://modding-openmw.gitlab.io/ncgdmw-lua/"
authors = [
	"johnnyhostile",
    "mym",
]
version = "$(git describe --tags --always)"
EOF

cleanup() {
    for f in CHANGELOG.md FAQ.md LICENSE README.md; do
        mv "00 Core/docs/$f" .
    done
    exit $1
}

for f in CHANGELOG.md FAQ.md LICENSE README.md; do
    mv $f "00 Core/docs/"
done

zip --must-match \
    --recurse-paths\
    --verbose \
    $file_name \
    "00 Core" \
    "test" \
    $modname-metadata.toml \
    --exclude ./00\ Core/ncgdmw.yaml \
    --exclude ./test/\*.yaml \
    || cleanup $?
sha256sum $file_name > $file_name.sha256sum.txt
sha512sum $file_name > $file_name.sha512sum.txt

cleanup 0
